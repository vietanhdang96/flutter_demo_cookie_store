import 'package:demo_cookie_store/cookie_detail.dart';
import 'package:flutter/material.dart';

class CookiePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 15.0),
          Container(
            padding: EdgeInsets.only(right: 15.0),
            width: MediaQuery.of(context).size.width - 30,
            height: MediaQuery.of(context).size.height - 50,
            child: GridView.count(
              crossAxisCount: 2,
              primary: false,
              crossAxisSpacing: 10.0,
              mainAxisSpacing: 15.0,
              childAspectRatio: 0.8,
              children: <Widget>[
                _buildCard('Cookie mint', '\$3.99', 'assets/cookiemint.jpg',
                    false, true, context),
                _buildCard('Cookie cream', '\$5.99', 'assets/cookiecream.jpg',
                    true, false, context),
                _buildCard('Cookie classic', '\$1.99',
                    'assets/cookieclassic.jpg', false, true, context),
                _buildCard('Cookie choco', '\$2.99', 'assets/cookiechoco.jpg',
                    true, false, context),
              ],
            ),
          ),
          SizedBox(height: 15.0)
        ],
      ),
    );
  }

  Widget _buildCard(String name, String price, String imagePath, bool added,
      bool isFavorite, context) {
    return Padding(
      padding: EdgeInsets.only(top: 0.0, bottom: 5.0, left: 5.0, right: 5.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => CookieDetail(
                  assetPath: imagePath, cookieName: name, cookiePrice: price)));
        },
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 3.0,
                    blurRadius: 5.0)
              ],
              color: Colors.white),
          child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      isFavorite
                          ? Icon(Icons.favorite, color: Colors.blue)
                          : Icon(Icons.favorite_border, color: Colors.blue)
                    ],
                  )),
              Hero(
                  tag: imagePath,
                  child: Container(
                    height: 75.0,
                    width: 75.0,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage(imagePath), fit: BoxFit.contain)),
                  )),
              SizedBox(height: 7.0),
              Text(price,
                  style: TextStyle(
                      color: Colors.blue,
                      fontFamily: 'Varela',
                      fontSize: 14.0)),
              Text(name,
                  style: TextStyle(
                      color: Colors.blue,
                      fontFamily: 'Varela',
                      fontSize: 14.0)),
              Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Container(color: Colors.grey, height: 1.0)),
              Padding(
                padding: EdgeInsets.only(left: 5.0, right: 5.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      if (!added) ...[
//                        Icon(Icons.shopping_basket, color: Colors.blue),
//                        Text('Add to cart',
//                            style: TextStyle(
//                                fontFamily: 'Varela',
//                                color: Colors.blueGrey,
//                                fontSize: 12.0))
//                      ],
//                    ],
                  children: List.unmodifiable(() sync* {
                    if (!added) {
                      yield Icon(Icons.shopping_basket, color: Colors.blue);
                      yield Text('Add to cart',
                          style: TextStyle(
                              fontFamily: 'Varela',
                              color: Colors.blueGrey,
                              fontSize: 12.0));
                    } else {
                      yield Icon(Icons.remove_circle_outline,
                          color: Colors.blue);
                      yield Text('8',
                          style: TextStyle(
                              fontFamily: 'Varela',
                              color: Colors.blueGrey,
                              fontWeight: FontWeight.bold,
                              fontSize: 12.0));
                      yield Icon(Icons.add_circle_outline, color: Colors.blue);
                    }
                  }()),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
